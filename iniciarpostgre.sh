 #!/bin/bash
opcao=$(whiptail --title "Opções PostgreSQL" --radiolist \
"Serviço PostgreSQL: " 15 50 5 \
"Parar" "Parar o serviço PostgreSQL" ON \
"Iniciar" "Iniciar o serviço PostgreSQL" OFF \
"Reiniciar" "Reiniciar o serviço PostgreSQL" OFF 3>&1 1>&2 2>&3)
status=$?
 
if [ $opcao == "Parar" ] 
then
	echo "Parando Serviço PostgreSQL..."
	sudo /etc/init.d/postgresql stop
elif [ $opcao ==  "Iniciar" ] 
then
	echo "Iniciando Serviço PostgreSQL..."
	sudo /etc/init.d/postgresql start
elif [ $opcao == "Reiniciar" ] 
then
	echo "Reiniciando Serviço PostgreSQL..."
	sudo /etc/init.d/postgresql restart
fi