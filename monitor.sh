#!/bin/bash


opcao=$(whiptail --title "Opções de monitor" --radiolist \
"Monitor: " 15 50 5 \
"1" "Um monitor" ON \
"2" "Dois Monitores" OFF 3>&1 1>&2 2>&3)
status=$?
 
if [ $opcao == "1" ] 
then
	sudo xrandr --output LVDS1 --mode 1366x768 --rate 60


#  Deixa o monitor para a esquerda do notebook:
 	sudo xrandr --output VGA1 --left-of LVDS1
#  Deixa o monitor para a direita do notebook:
#xrandr --output LVDS --left-of VGA-0

# CONFIGURAR O MONITOR PRINCIPAL:
# Faz com que o monitor do notebook seja o principal:
	sudo xrandr --output LVDS1 --primary
# Faz com que o monitor comum seja o principal:
# xrandr --output VGA1 --primary
	sudo xrandr --output VGA1 --off
else

# Saída configura o monitor em 1600x900px e rate de 70hz:
sudo xrandr --output VGA1 --mode 1600x900 --rate 70
# Saída do notebook em 1366x768px com rate de 60hz:
sudo xrandr --output LVDS1 --mode 1366x768 --rate 60


#  Deixa o monitor para a esquerda do notebook:
#sudo xrandr --output VGA1 --left-of LVDS1
sudo xrandr --output VGA1 --right-of LVDS1

#  Deixa o monitor para a direita do notebook:
#xrandr --output LVDS --left-of VGA-0

# CONFIGURAR O MONITOR PRINCIPAL:
# Faz com que o monitor do notebook seja o principal:
#sudo xrandr --output LVDS1 --primary
# Faz com que o monitor comum seja o principal:
sudo xrandr --output VGA1 --primary


fi
 
