#!/bin/bash


opcao=$(whiptail --title "Opções de teclado" --radiolist \
"Monitor: " 15 50 5 \
"1" "US Internacional" ON \
"2" "BR ABNT2" OFF 3>&1 1>&2 2>&3)
status=$?
 
if [ $opcao == "1" ] 
then
echo "XKBMODEL=\"pc105\"
XKBLAYOUT=\"us\"
XKBVARIANT=\"intl\"
XKBOPTIONS=\"lv3:alt_switch,compose:rctrl\"" | sudo tee  /etc/default/keyboard
else
echo "XKBMODEL=\"pc105\"
XKBLAYOUT=\"br\"
XKBVARIANT=\"\"
XKBOPTIONS=\"lv3:alt_switch,compose:rctrl\"" | sudo tee  /etc/default/keyboard 

fi
 

sudo dpkg-reconfigure keyboard-configuration
